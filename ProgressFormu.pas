unit ProgressFormu;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls;

type
  TProgressForm = class(TForm)
    ProgressBar1: TProgressBar;
    Label1: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetProgress(Percent: Integer);
  end;

var
  ProgressForm: TProgressForm;

implementation

{$R *.dfm}

{ TForm2 }

procedure TProgressForm.SetProgress(Percent: Integer);
begin
  if (Percent < 0) or (Percent > 100) then
    raise Exception.Create('Invalid progress parameter');
  ProgressBar1.Position := Percent;
  Application.ProcessMessages;
end;

end.
