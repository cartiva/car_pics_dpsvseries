object ConfirmUpload: TConfirmUpload
  Left = 0
  Top = 0
  Caption = 'Confirm Picture Upload'
  ClientHeight = 514
  ClientWidth = 490
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    490
    514)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 380
    Width = 474
    Height = 33
    Alignment = taCenter
    Anchors = [akLeft, akRight, akBottom]
    AutoSize = False
    Caption = 'Label1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    WordWrap = True
    ExplicitTop = 354
    ExplicitWidth = 685
  end
  object Image1: TImage
    Left = 40
    Top = 14
    Width = 400
    Height = 300
    Anchors = [akLeft, akTop, akRight, akBottom]
    Stretch = True
  end
  object VehicleDescription: TLabel
    Left = 8
    Top = 320
    Width = 474
    Height = 54
    Alignment = taCenter
    Anchors = [akLeft, akRight, akBottom]
    AutoSize = False
    Caption = 'Label1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    WordWrap = True
  end
  object Button1: TButton
    Left = 326
    Top = 481
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    TabOrder = 0
    OnClick = Button1Click
    ExplicitLeft = 537
    ExplicitTop = 455
  end
  object Button2: TButton
    Left = 407
    Top = 481
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
    ExplicitLeft = 618
    ExplicitTop = 455
  end
  object CheckBox1: TCheckBox
    Left = 8
    Top = 436
    Width = 477
    Height = 39
    Anchors = [akLeft, akRight, akBottom]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    WordWrap = True
    ExplicitTop = 410
    ExplicitWidth = 688
  end
end
