object LoginForm: TLoginForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Car Pic Login'
  ClientHeight = 131
  ClientWidth = 309
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 11
    Width = 48
    Height = 13
    Caption = 'Username'
  end
  object Label2: TLabel
    Left = 16
    Top = 38
    Width = 46
    Height = 13
    Caption = 'Password'
  end
  object MessageLabel: TLabel
    Left = 16
    Top = 62
    Width = 275
    Height = 25
    Alignment = taCenter
    AutoSize = False
    WordWrap = True
  end
  object UserNameEdit: TEdit
    Left = 80
    Top = 8
    Width = 153
    Height = 21
    TabOrder = 0
  end
  object PasswordEdit: TEdit
    Left = 80
    Top = 35
    Width = 153
    Height = 21
    PasswordChar = '*'
    TabOrder = 1
  end
  object Button1: TButton
    Left = 128
    Top = 93
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 216
    Top = 93
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
end
