unit GetVehicleu;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DB, Grids, DBGrids;

type
  TSelectVehicleForm = class(TForm)
    Label1: TLabel;
    StockNumberEdit: TEdit;
    SearchStockNumberButton: TButton;
    SearchVinButton: TButton;
    Label2: TLabel;
    VINEdit: TEdit;
    Button3: TButton;
    Button4: TButton;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    FuzzySearchCheckBox: TCheckBox;
    MessageLabel: TLabel;
    procedure SearchStockNumberButtonClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure SearchVinButtonClick(Sender: TObject);
    procedure StockNumberEditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure VINEditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure GetVehiclesBasedOnStockNumber;
    procedure GetVehiclesBasedOnVIN;
  end;

var
  SelectVehicleForm: TSelectVehicleForm;

implementation

uses datamodu, mainformu;

{$R *.dfm}

procedure TSelectVehicleForm.SearchStockNumberButtonClick(Sender: TObject);
begin
  MessageLabel.Caption := '';
  if StockNumberEdit.Text = '' then
    ShowMessage('Enter a stock number to search on')
  else
  begin
    try
      Screen.Cursor := crHourGlass;
      GetVehiclesBasedOnStockNumber;
    finally
      Screen.Cursor := crDefault;
    end;
    case DataModule1.AdsQuery1.RecordCount of
      0:
      begin
        MessageLabel.Caption := 'No matches found for ' + StockNumberEdit.Text;
        Button3.Enabled := False;
      end;
      1:
      begin
        DbGrid1.Visible := True;
        DbGrid1.SetFocus;
        MessageLabel.Caption := 'There was one match. Click OK to continue or change your search';
        Self.Button3.Enabled := True;
      end;
      2..MaxInt:
      begin
        Self.Button3.Enabled := True;
        DbGrid1.Visible := True;
        MessageLabel.Caption := 'There were ' + IntToStr(DataModule1.AdsQuery1.RecordCount) +
          ' matches. Select your vehicle from the following list and click OK, or change your search';
      end;
    end;
  end;
end;

procedure TSelectVehicleForm.SearchVinButtonClick(Sender: TObject);
begin
  MessageLabel.Caption := '';
  if VINEdit.Text = '' then
    ShowMessage('Enter a VIN to search on')
  else
  begin
    try
      Screen.Cursor := crHourGlass;
      GetVehiclesBasedOnVIN;
    finally
      Screen.Cursor := crDefault;
    end;
    case DataModule1.AdsQuery1.RecordCount of
      0:
      begin
        Self.Button3.Enabled := False;
        MessageLabel.Caption := 'No matches found for ' + VINEdit.Text;
      end;
      1:
      begin
        DbGrid1.Visible := True;
        DbGrid1.SetFocus;
        Self.Button3.Enabled := True;
        MessageLabel.Caption := 'There was one match. Click OK to continue or change your search';
      end;
      2..MaxInt:
      begin
        DbGrid1.Visible := True;
        Self.Button3.Enabled := True;
        MessageLabel.Caption := 'There were ' + IntToStr(DataModule1.AdsQuery1.RecordCount) +
          ' matches. Select your vehicle from the following list and click OK, or change your search';
      end;
    end;
  end;
end;

procedure TSelectVehicleForm.StockNumberEditKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
if Key = VK_RETURN then
begin
  Key := 0;
  SearchStockNumberButton.SetFocus;
  SearchStockNumberButtonClick(Sender);
end;
end;

procedure TSelectVehicleForm.VINEditKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if Key = VK_RETURN then
begin
  Key := 0;
  SearchVinButton.SetFocus;
  SearchVinButtonClick(Sender);
end;
end;

procedure TSelectVehicleForm.Button3Click(Sender: TObject);
begin
  if not DataModule1.AdsQuery1.Active then
  begin
    ShowMessage('Search for a vehicle or select Cancel');
    exit;
  end;
  if DataModule1.AdsQuery1.IsEmpty then
  begin
    ShowMessage('Select a vehicle before clicking OK');
    exit;
  end;
  if DataModule1.AdsQuery1.FieldByName('CurrentPictureCount').AsInteger > 0 then
  begin
   if MessageDlg('There are already pictures for this vehicle. If this is correct, select OK. ' +
                 'Otherwsise, select cancel to return to the Select Vehicle form',
                 mtConfirmation, [mbOK,mbCancel], 0) <> mrOK then exit;
{ TODO : 8/24/12
  cary, why would you ever want to delete the files that have been uploaded
  what if you are adding on to a vehicle, new pictures have been uploaded prior
  to searching for vehicle, find the vehicle, acknowledge that picures exist
  and boom, this will delete the files you just uploaded
  IF CleanupFiles = 1, either by the ini or a previous operation (check box on
  dialogs) that set it to 1
  leaving it in for now, but setting ini = 0 on production box}
   if Form1.CleanupFiles then Form1.EmptyCurrentDir;
  end;
  ModalResult := mrOK
end;

procedure TSelectVehicleForm.GetVehiclesBasedOnStockNumber;
var
  SqlStmt: String;
begin
  if FuzzySearchCheckBox.Checked or (Length(StockNumberEdit.Text) <= 3) then
  begin
    SqlStmt :=
      'SELECT vii.StockNumber, vii.VehicleInventoryItemID, vi.Vin, vi.VehicleItemID, '+
            'trim(Coalesce(vii.StockNumber, '''')) + '' '' + Coalesce(vi.YearModel, '''') + '' '' + Trim(Coalesce(vi.Make, '''')) + ' +
            ''' '' + Trim(Coalesce(vi.Model, '''')) + '' '' + Trim(Coalesce(vi.Trim, '''')) + '' '' + ' +
            'Trim(Coalesce(vi.BodyStyle, '''')) + '' '' + Trim(Coalesce(vi.ExteriorColor, '''')) As Description, ' +
//NEW LINE
            '(SELECT Count(*) FROM [' + DataModule1.VehiclePictureTableName + '] WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID) as CurrentPictureCount ' +
      'FROM VehicleInventoryItems vii ' +
      'INNER JOIN VehicleItems vi ON vi.VehicleItemID = vii.VehicleItemID ' +
      'WHERE UCase(vii.StockNumber) LIKE UCase(:sn) ';
    DataModule1.AdsQuery1.SQL.Text := SqlStmt;
    DataModule1.AdsQuery1.Params[0].AsString := '%' + StockNumberEdit.Text + '%';
  end
  else
  begin
    SqlStmt :=
      'SELECT vii.StockNumber, vii.VehicleInventoryItemID, vi.Vin, vi.VehicleItemID, '+
            'trim(Coalesce(vii.StockNumber, '''')) + '' '' + Coalesce(vi.YearModel, '''') + '' '' + Trim(Coalesce(vi.Make, '''')) + ' +
            ''' '' + Trim(Coalesce(vi.Model, '''')) + '' '' + Trim(Coalesce(vi.Trim, '''')) + '' '' + ' +
            'Trim(Coalesce(vi.BodyStyle, '''')) + '' '' + Trim(Coalesce(vi.ExteriorColor, '''')) As Description, ' +
//NEW LINE
            '(SELECT Count(*) FROM [' + DataModule1.VehiclePictureTableName + '] WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID) as CurrentPictureCount ' +
      'FROM VehicleInventoryItems vii ' +
      'INNER JOIN VehicleItems vi ON vi.VehicleItemID = vii.VehicleItemID ' +
      'WHERE UCase(vii.StockNumber) = UCase(:sn) ';
    DataModule1.AdsQuery1.SQL.Text := SqlStmt;
    DataModule1.AdsQuery1.Params[0].AsString := StockNumberEdit.Text;
  end;
  DataModule1.AdsQuery1.Open;
  Self.DataSource1.DataSet := DataModule1.AdsQuery1;
end;

procedure TSelectVehicleForm.GetVehiclesBasedOnVIN;
var
  SqlStmt: String;
begin
  if FuzzySearchCheckBox.Checked or (Length(VINEdit.Text) <= 17) then
  begin
    SqlStmt :=
      'SELECT vii.StockNumber, vii.VehicleInventoryItemID, vi.Vin, vi.VehicleItemID, '+
            'trim(Coalesce(vii.StockNumber, '''')) + '' '' + Coalesce(vi.YearModel, '''') + '' '' + Trim(Coalesce(vi.Make, '''')) + ' +
            ''' '' + Trim(Coalesce(vi.Model, '''')) + '' '' + Trim(Coalesce(vi.Trim, '''')) + '' '' + ' +
            'Trim(Coalesce(vi.BodyStyle, '''')) + '' '' + Trim(Coalesce(vi.ExteriorColor, '''')) As Description, ' +
//NEW LINE
            '(SELECT Count(*) FROM [' + DataModule1.VehiclePictureTableName + '] WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID) as CurrentPictureCount ' +
      'FROM VehicleInventoryItems vii ' +
      'INNER JOIN VehicleItems vi ON vi.VehicleItemID = vii.VehicleItemID ' +
      'WHERE SUBSTRING(vi.VIN, 1, ' +
      IntToStr(Length(VINEdit.Text)) + ') LIKE :vin;';
    DataModule1.AdsQuery1.SQL.Text := SqlStmt;
    DataModule1.AdsQuery1.Params[0].AsString := '%' + VINEdit.Text + '%';
  end
  else
  begin
    SqlStmt :=
      'SELECT vii.StockNumber, vii.VehicleInventoryItemID, vi.Vin, vi.VehicleItemID, '+
            'trim(Coalesce(vii.StockNumber, '''')) + '' '' + Coalesce(vi.YearModel, '''') + '' '' + Trim(Coalesce(vi.Make, '''')) + ' +
            ''' '' + Trim(Coalesce(vi.Model, '''')) + '' '' + Trim(Coalesce(vi.Trim, '''')) + '' '' + ' +
            'Trim(Coalesce(vi.BodyStyle, '''')) + '' '' + Trim(Coalesce(vi.ExteriorColor, '''')) As Description, ' +
//NEW LINE
            '(SELECT Count(*) FROM [' + DataModule1.VehiclePictureTableName + '] WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID) as CurrentPictureCount ' +
      'FROM VehicleInventoryItems vii ' +
      'INNER JOIN VehicleItems vi ON vi.VehicleItemID = vii.VehicleItemID ' +
      'WHERE VIN = :vin';
    DataModule1.AdsQuery1.SQL.Text := SqlStmt;
    DataModule1.AdsQuery1.Params[0].AsString := VINEdit.Text;
  end;
  DataModule1.AdsQuery1.Open;
  Self.DataSource1.DataSet := DataModule1.AdsQuery1;
end;

end.
