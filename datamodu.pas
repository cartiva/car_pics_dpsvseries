unit datamodu;

interface

uses
  SysUtils, Classes, DB, adsdata, adsfunc, adstable, adscnnct, adsset;

type
  TDataModule1 = class(TDataModule)
    AdsConnection1: TAdsConnection;
    AdsQuery1: TAdsQuery;
    AdsTable1: TAdsTable;
  private
    { Private declarations }
    FConnection: String;
    FVehiclePictureTableName: String;
    FVehicleSequenceTableName: String;
    FVehicleUploadsTableName: String;
    procedure SetVehiclePictureTableName(const Value: String);
    procedure SetVehicleSequenceTableName(const Value: String);
    procedure SetVehicleUploadsTableName(const Value: String);
    const
      password = 'cartiva';
      username = 'adssys';
  public
    { Public declarations }
    property Connection: String read FConnection write FConnection;
    property VehiclePictureTableName: String read FVehiclePictureTableName write SetVehiclePictureTableName;
    property VehicleSequenceTableName: String read FVehicleSequenceTableName write SetVehicleSequenceTableName;
    property VehicleUploadsTableName: String read FVehicleUploadsTableName write SetVehicleUploadsTableName;
    procedure MakeConnection;
  end;

var
  DataModule1: TDataModule1;

implementation

{$R *.dfm}

procedure TDataModule1.MakeConnection;
begin
  AdsConnection1.ConnectPath := FConnection;
  AdsConnection1.Username := username;
  AdsConnection1.Password := password;
//  AdsConnection1.AdsServerTypes :=  [stADS_AIS];
  AdsConnection1.AdsServerTypes :=  [stADS_Remote];
  AdsConnection1.Connect;
end;

procedure TDataModule1.SetVehiclePictureTableName(const Value: String);
begin
  FVehiclePictureTableName := Value;
end;

procedure TDataModule1.SetVehicleSequenceTableName(const Value: String);
begin
  FVehicleSequenceTableName := Value;
end;

procedure TDataModule1.SetVehicleUploadsTableName(const Value: String);
begin
  FVehicleUploadsTableName := Value;
end;

end.
