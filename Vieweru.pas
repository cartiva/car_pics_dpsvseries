unit Vieweru;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, PictureImage;

type
  TViewer = class(TForm)
    Image1: TImage;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    class procedure ShowPicture(Image: TImage);
  end;

var
  Viewer: TViewer;

implementation

{$R *.dfm}

{ TViewer }


{ TViewer }

procedure TViewer.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

class procedure TViewer.ShowPicture(Image: TImage);
begin
  with TViewer.Create(nil) do
  begin
     ClientHeight := Image.Picture.Height;
     ClientWidth := Image.Picture.Width;
     Image1.Picture.Assign(Image.Picture);
     Show;
  end;
end;

end.
