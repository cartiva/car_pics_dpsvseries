unit LoginFormu;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TLoginForm = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    UserNameEdit: TEdit;
    PasswordEdit: TEdit;
    MessageLabel: TLabel;
    Button1: TButton;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
  private
    FUserID: String;
    { Private declarations }
    function IsValidCarPicManager(UserName: String; Password: String): Boolean;
  public
    { Public declarations }
    property UserID: String read FUserID write FUserID;
  end;

var
  LoginForm: TLoginForm;

implementation

uses datamodu;

{$R *.dfm}

var
  Retries: Integer = 0;
const
  RetryCount = 3;


procedure TLoginForm.Button1Click(Sender: TObject);
begin
  if not IsValidcarPicManager(UserNameEdit.Text, PasswordEdit.Text) then
  begin
    inc(Retries);
    if Retries >= RetryCount then
    begin
      ShowMessage('Access denied');
      ModalResult := mrCancel;
      exit;
    end
    else
    begin
      MessageLabel.Caption := '';
      Application.ProcessMessages;
      sleep(1000);
      MessageLabel.Caption := 'Username and/or password is invalid, or you do not have access to the CarPic application';
      PasswordEdit.Text := '';
      PasswordEdit.SetFocus;
      exit;
    end;
  end;
  ModalResult := mrOK;
end;


function TLoginForm.IsValidCarPicManager(UserName, Password: String): Boolean;
begin
  DataModule1.AdsQuery1.SQL.Text :=
  'select ' +
  '  case when ' +
  '  (SELECT 1 ' +
	'  from Users u ' +
	'  inner join ApplicationUsers au ' +
	'    on au.PartyID = u.PartyID ' +
	'	 and au.AppName = ''carpic'' ' +
	'	 and au.ThruTS is null ' +
	'  where u.UserName = :un ' +
	'    and u.Password = :pass ' +
	'	   and u.Active = True) is not null ' +
	'  then True ' +
	' else False ' +
  'end, (SELECT ut.PartyID FROM Users ut WHERE ut.UserName = :un and ut.Password = :pass) as PartyID ' +
  'from system.iota; ';
  DataModule1.AdsQuery1.Params[0].AsString := Username;
  DataModule1.AdsQuery1.Params[1].AsString := Password;
  DataModule1.AdsQuery1.Open;
  Result := DataModule1.AdsQuery1.Fields[0].AsBoolean;
  if Result then FUserID := DataModule1.AdsQuery1.Fields[1].AsString;
  DataModule1.AdsQuery1.Close;
end;

end.
