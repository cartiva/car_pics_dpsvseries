program CarPics;

uses
  Forms,
  mainformu in 'mainformu.pas' {Form1},
  Vieweru in 'Vieweru.pas' {Viewer},
  ProgressFormu in 'ProgressFormu.pas' {ProgressForm},
  ProgressThreadu in 'ProgressThreadu.pas',
  datamodu in 'datamodu.pas' {DataModule1: TDataModule},
  GetVehicleu in 'GetVehicleu.pas' {SelectVehicleForm},
  ConfirmUploadu in 'ConfirmUploadu.pas' {ConfirmUpload},
  Differentiatoru in 'Differentiatoru.pas' {Differentiator},
  LoginFormu in 'LoginFormu.pas' {LoginForm},
  UploadCompleteu in 'UploadCompleteu.pas' {UploadComplete},
  DeletePicturesu in 'DeletePicturesu.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.ShowMainForm := False;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TLoginForm, LoginForm);
  Application.Run;
end.
