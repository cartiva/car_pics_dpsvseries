object SelectVehicleForm: TSelectVehicleForm
  Left = 0
  Top = 0
  Caption = 'Select Vehicle'
  ClientHeight = 377
  ClientWidth = 638
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    638
    377)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 15
    Top = 19
    Width = 66
    Height = 13
    Caption = 'Stock Number'
  end
  object Label2: TLabel
    Left = 15
    Top = 45
    Width = 85
    Height = 26
    Caption = 'VIN (at least first six characters)'
    WordWrap = True
  end
  object MessageLabel: TLabel
    Left = 11
    Top = 112
    Width = 615
    Height = 14
    AutoSize = False
    WordWrap = True
  end
  object StockNumberEdit: TEdit
    Left = 106
    Top = 15
    Width = 121
    Height = 21
    TabOrder = 0
    OnKeyUp = StockNumberEditKeyUp
  end
  object SearchStockNumberButton: TButton
    Left = 364
    Top = 8
    Width = 144
    Height = 25
    Caption = 'Search By Stock Number'
    TabOrder = 1
    OnClick = SearchStockNumberButtonClick
  end
  object SearchVinButton: TButton
    Left = 364
    Top = 39
    Width = 144
    Height = 25
    Caption = 'Search by VIN'
    TabOrder = 2
    OnClick = SearchVinButtonClick
  end
  object VINEdit: TEdit
    Left = 106
    Top = 42
    Width = 232
    Height = 21
    TabOrder = 3
    OnKeyUp = VINEditKeyUp
  end
  object Button3: TButton
    Left = 474
    Top = 344
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    Enabled = False
    TabOrder = 4
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 555
    Top = 344
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 5
  end
  object DBGrid1: TDBGrid
    Left = 8
    Top = 132
    Width = 622
    Height = 194
    Anchors = [akLeft, akTop, akBottom]
    DataSource = DataSource1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 6
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Visible = False
    Columns = <
      item
        Expanded = False
        FieldName = 'StockNumber'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Description'
        Width = 342
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Vin'
        Width = 166
        Visible = True
      end>
  end
  object FuzzySearchCheckBox: TCheckBox
    Left = 17
    Top = 81
    Width = 321
    Height = 17
    Caption = 'Perform Fuzzy Search (exact match not required)'
    Checked = True
    State = cbChecked
    TabOrder = 7
  end
  object DataSource1: TDataSource
    DataSet = DataModule1.AdsQuery1
    Left = 318
    Top = 7
  end
end
