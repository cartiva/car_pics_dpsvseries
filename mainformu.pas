unit mainformu;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, jpeg, ImgList, Menus, ComCtrls, SyncObjs, db, ActiveX,
  PictureImage, Buttons, Character, FileCtrl;

type
  TForm1 = class(TForm)
    OpenDialog1: TOpenDialog;
    DirEdit: TEdit;
    Button1: TButton;
    ListBox1: TListBox;
    PopupMenu1: TPopupMenu;
    Panel1: TPanel;
    Label35: TLabel;
    MainMenu1: TMainMenu;
    Panel2: TPanel;
    RemoveImage1: TMenuItem;
    DisplayImageFullSize1: TMenuItem;
    Button2: TButton;
    Label34: TLabel;
    VehicleDescriptionLabel: TLabel;
    SelectVehicleButton: TButton;
    File1: TMenuItem;
    Exit1: TMenuItem;
    Pictures1: TMenuItem;
    Upload1: TMenuItem;
    RemoveAllImages1: TMenuItem;
    GridPanel1: TGridPanel;
    LoadorReloadFiles1: TMenuItem;
    N1: TMenuItem;
    PictureImage1: TPictureImage;
    PictureImages: TImageList;
    PictureImage2: TPictureImage;
    PictureImage3: TPictureImage;
    PictureImage4: TPictureImage;
    PictureImage5: TPictureImage;
    PictureImage6: TPictureImage;
    PictureImage7: TPictureImage;
    PictureImage8: TPictureImage;
    PictureImage9: TPictureImage;
    PictureImage10: TPictureImage;
    PictureImage11: TPictureImage;
    PictureImage12: TPictureImage;
    PictureImage13: TPictureImage;
    PictureImage14: TPictureImage;
    PictureImage15: TPictureImage;
    PictureImage16: TPictureImage;
    PictureImage17: TPictureImage;
    PictureImage18: TPictureImage;
    PictureImage19: TPictureImage;
    PictureImage20: TPictureImage;
    PictureImage21: TPictureImage;
    PictureImage22: TPictureImage;
    PictureImage23: TPictureImage;
    PictureImage24: TPictureImage;
    PictureImage25: TPictureImage;
    PictureImage26: TPictureImage;
    PictureImage27: TPictureImage;
    PictureImage28: TPictureImage;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    Pictures2: TMenuItem;
    N2: TMenuItem;
    ManageRydellDifferentiatorPictures1: TMenuItem;
    N3: TMenuItem;
    ClearAll1: TMenuItem;
    LoadedPictureLabel: TLabel;
    BitBtn4: TBitBtn;
    DiffPictureImage: TPictureImage;
    PopupMenu2: TPopupMenu;
    DisplayImageFullSize2: TMenuItem;
    BitBtn5: TBitBtn;
    N4: TMenuItem;
    DeletePicturesfromVehicle1: TMenuItem;
//    EmptyPictureDirBtn: TButton;  // 8/23/12 removed because it simply does not
//      work, procedure always returns negative dialog even when no pics have
//      been selected
    StatusBar1: TStatusBar;
    procedure Button1Click(Sender: TObject);
    procedure PicImageDragDrop(Sender, Target: TObject; X, Y: Integer);
    procedure PicImage1DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure RecycleBinDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure RecycleBinDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DisplayImageFullSize1Click(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure ListBox1MeasureItem(Control: TWinControl; Index: Integer;
      var Height: Integer);
    procedure ListBox1DrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure Button2Click(Sender: TObject);
    procedure LoadFilesButtonClick(Sender: TObject);
    procedure SelectVehicleButtonClick(Sender: TObject);
    procedure Upload1Click(Sender: TObject);
    procedure RemoveImage1Click(Sender: TObject);
    procedure RemoveAllImages1Click(Sender: TObject);
    procedure LoadorReloadFiles1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Clic(Sender: TObject);
    procedure ListBox1KeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Exit1Click(Sender: TObject);
    procedure ManageRydellDifferentiatorPictures1Click(Sender: TObject);
    procedure ClearAll1Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure DisplayImageFullSize2Click(Sender: TObject);
    procedure SequenceMenuItemClick(Sender: TObject);
    procedure DeletePicturesfromVehicle1Click(Sender: TObject);
//    procedure EmptyPictureDirBtnClick(Sender: TObject);
  private
    { Private declarations }
    SequenceNames: array of array of String;
    PictureList: array [1..28] of String;
    FPictureDirectoryChanged: Boolean;
    FCurrentVIN: String;
    FCurrentVehicleInventoryItemID: String;
    FCurrentVehicleItemID: String;
    FCurrentDescription: String;
    FUserID: String;
    SequenceList: TStringList;
    DescriptionList: TStringList;
    Path: String;
    LoadType: String;
    RepresentativePictureNo: Integer;
    function GetIntFromPicImage(Name: String): Integer;
    function get_PictureDirectory: String;
    procedure set_PictureDirectory(Value: String);
    function LookupVehicle: Boolean;
    procedure ProcessVehicle;
    procedure LoadSequenceList;
    procedure LoadSequenceNames;
    procedure LoadFiles;
    procedure LoadDifferentiator;
    procedure DoUpload;
    procedure DoDelete;
  public
    { Public declarations }
    CleanupFiles: Boolean;
    VehicleWithExistingPictures: Boolean;
    DefaultLoadType: string;
    procedure EmptyCurrentDir;
    procedure GetDirectoryFiles;
    property PictureDirectory: String read get_PictureDirectory write set_PictureDirectory;
    function JPegToTPicture(JPegName: String; out Picture: TPicture): Boolean;
    procedure ClearAll(Reset: Boolean = False);
    procedure InitializeSequenceMenuItems;
    procedure ToggleSetTypeMenuItems;
    function MakeReadableMenuName(const Name: String): String;
  end;

function ResizeJPG2BMP(Filename: String; w,h: Integer): TBitmap;

function GuidAsString: String;

var
  Form1: TForm1;
  ProgressFormProgress: Integer;
  ProgressEvent: TEvent;
  DirName: String;

const
  wConst = 40;
  hConst = 30;
  TempDir = 'pictemp';

implementation

{$R *.dfm}

uses inifiles, Vieweru, ProgressFormu, ProgressThreadu, datamodu, GetVehicleu,
  UploadCompleteu, Differentiatoru, LoginFormu, DeletePicturesu, ConfirmUploadu;

resourcestring
  NoVehicleSelected = 'No Vehicle Selected';


function ResizeJPG2BMP(Filename: String; w,h: Integer): TBitmap;
var
  BitmapOriginal:  TBitmap;
  BitmapNew    :  TBitmap;
  function GetJpegAsBmp(JpgFileName: string): TBitmap;
  var
    Bmp: TBitmap;
    Jpg: TJPEGImage;
  begin
    Bmp := TBitmap.Create;
    Jpg := TJPEGImage.Create;
    try
      Jpg.LoadFromFile(JpgFileName);
      Bmp.Assign(Jpg);
      Result := Bmp;
    finally
      Jpg.Free;
    end;
  end;
begin
  BitmapOriginal := GetJPegAsBmp(Filename);
  try
    BitmapNew := TBitmap.Create;
    BitmapNew.Width  := w;
    BitmapNew.Height := h;
    BitmapNew.PixelFormat := BitmapOriginal.PixelFormat;
    BitmapNew.Canvas.StretchDraw(BitmapNew.Canvas.ClipRect,
                                  BitmapOriginal);
    Result := BitmapNew
  finally
    BitmapOriginal.Free
  end;
end;

function GuidAsString: String;
var
  aGUID : TGUID;
begin
  if CreateGUID( aGUID ) = 0 then
    Result := GuidToString(aGuid);
end;

procedure TForm1.BitBtn2Click(Sender: TObject);
begin
  if ListBox1.ItemIndex = -1 then exit;
  if ListBox1.ItemIndex = 0 then exit
  else
  begin
    PictureImages.Move(Listbox1.ItemIndex, ListBox1.ItemIndex - 1);
    ListBox1.Items.Exchange(ListBox1.ItemIndex, ListBox1.ItemIndex - 1);
  end;
end;

procedure TForm1.BitBtn3Clic(Sender: TObject);
begin
  if ListBox1.ItemIndex = -1 then exit;
  if ListBox1.ItemIndex = (ListBox1.Count -1) then exit
  else
  begin
    PictureImages.Move(Listbox1.ItemIndex, ListBox1.ItemIndex + 1);
    ListBox1.Items.Exchange(ListBox1.ItemIndex, ListBox1.ItemIndex + 1);
  end;
end;

procedure TForm1.BitBtn4Click(Sender: TObject);
var
  CurrentItem: Integer;
begin
  if ListBox1.ItemIndex = -1 then exit
  else
  begin
    CurrentItem := Listbox1.ItemIndex;
    PictureImages.Delete(Listbox1.ItemIndex);
    ListBox1.Items.Delete(ListBox1.ItemIndex);
    if CurrentItem >= ListBox1.Count then
      ListBox1.ItemIndex := CurrentItem - 1
    else
      ListBox1.ItemIndex := CurrentItem;
  end;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  Dir: String;
begin
  if SelectDirectory('Select a directory', '', Dir) then
  begin
    DirEdit.Text := Dir;
    DirName := DirEdit.Text;
    if VehicleDescriptionLabel.Caption <> NoVehicleSelected then
      LoadFiles;
  end;
end;

procedure TForm1.LoadFiles;
begin
  if VehicleDescriptionLabel.Caption = NoVehicleSelected then
  begin
    ShowMessage('Please selected a vehicle before loading pictures');
    exit;
  end;
  if (DirEdit.Text <> '') and SysUtils.DirectoryExists(DirEdit.Text) then
  try
    DirName := DirEdit.Text;
    StatusBar1.SimpleText := 'Getting Files and Images. This may take a while...';
    Screen.Cursor := crHourGlass;
    GetDirectoryFiles;
  finally
    Screen.Cursor := crDefault;
    StatusBar1.SimpleText := '';
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  i: Integer;
  Found: Boolean;
  Image: TImage;
  CurrentListCount: Integer;
begin
  if VehicleDescriptionLabel.Caption = NoVehicleSelected then
  begin
    ShowMessage('Please start by selecting a vehicle');
    exit;
  end;
  Found := False;
  for i := 1 to SequenceList.Count do
    if PictureList[i] <> '' then
    begin
      Found := False;
      break;
    end;
  if Found then
    if MessageDlg('Load All replaces current pictures with the current items in the File List. Select OK to replace, or Cancel',
         mtConfirmation, [mbOk, mbCancel], 0) <> mrOK then
      exit;
  //If Found = False or the user confirmed, continue
  CurrentListCount := SequenceList.Count;
  if ListBox1.Count < CurrentListCount then
    CurrentListCount := ListBox1.Count;

  for i := 1 to SequenceList.Count do
  begin
    //Remove all items from the Image controls
    //Add any existing items in the image controls to the end of the listbox list
    if PictureList[i] <> '' then
    begin
      TPictureImage(Self.FindComponent('PictureImage' + IntToStr(i))).Image.Picture := nil;
      ListBox1.Items.Add(ExtractFileName(PictureList[i]));
      PictureImages.Add(ResizeJPG2BMP(PictureList[i], wConst, hConst),nil);
    end;
  end;

  begin
    for i := 1 to CurrentListCount do
    begin
      Image := TPictureImage(Self.FindComponent('PictureImage' + IntToStr(i))).Image;
      PictureList[i] := DirName + '\' + ListBox1.Items[i - 1];
      Image.Picture.LoadFromFile(PictureList[i]);
    end;
    //Now that the pictures are loaded, remove those items from the ListBox
    for i := 1 to CurrentListCount do
    begin
      ListBox1.Items.Delete(0);
      PictureImages.Delete(0);
    end;
  end;
end;


procedure TForm1.EmptyCurrentDir;
var
    SearchRec: TSearchRec;
    Filename: String;
begin
  if not SysUtils.DirectoryExists(DirEdit.Text) then
  begin
    ShowMessage('Problem : ' + DirEdit.Text + ' does not exist. Cannot remove files');
    exit;
  end;

  if FindFirst(DirEdit.Text + '\*.*', faAnyFile, SearchRec) = 0 then
  begin
    FileName := SearchRec.Name;
    if (UPPERCASE(ExtractFileExt(Filename)) = '.JPG') or
       (UPPERCASE(ExtractFileExt(FileName)) = '.JPEG') then
      DeleteFile(DirName + '\' + FileName);
  end;
  while FindNext(SearchRec) = 0 do
  begin
    Filename := SearchRec.Name;
    if (UPPERCASE(ExtractFileExt(Filename)) = '.JPG') or
      (UPPERCASE(ExtractFileExt(FileName)) = '.JPEG') then
      DeleteFile(DirName + '\' + FileName);
  end;
  ListBox1.Items.Clear;
end;


//procedure TForm1.EmptyPictureDirBtnClick(Sender: TObject);
//var
//  PictureImage: TPictureImage;
//  i: Integer;
//begin
//  for i := 1 to 28 do
//  begin
//    PictureImage := TPictureImage(FindComponent('PictureImage' + IntToStr(i)));
//    if PictureImage <> nil then
//    begin
//      ShowMessage('You cannot empty the picture directory when one or more pictures have already been selected');
//      exit;
//    end;
//  end;
//
//if MessageDlg('Are you sure you want to empty the pictures from the picture directory?', mtConfirmation, mbOkCancel, 0) = mrOK then
//  EmptyCurrentDir;
//end;

procedure TForm1.ClearAll(Reset: Boolean);
var
  PictureImage: TPictureImage;
  i: Integer;

  procedure ClearVehicle;
  begin
    VehicleDescriptionLabel.Caption := NoVehicleSelected;
    DiffPictureImage.Visible := False;
    FCurrentDescription := '';
    FCurrentVehicleInventoryItemID := '';
    VehicleWithExistingPictures := False;
    FCurrentVehicleItemID := '';
    FCurrentVIN := '';
  end;
begin
  LoadedPictureLabel.Caption := '';
  for i := 1 to 28 do
  begin
    PictureImage := TPictureImage(FindComponent('PictureImage' + IntToStr(i)));
    if PictureImage <> nil then
    begin
      PictureImage.Image.Picture := nil;
      PictureImage.Caption := '';
      PictureList[i] := '';
    end;
  end;
  //Reset is true when a full clear is requested. When switching SetTypes, Reset is False
  if Reset then
  begin
    ClearVehicle;
    Upload1.Enabled := False;
    DeletePicturesfromVehicle1.Enabled := False;
    Self.ListBox1.Clear;
  end;

end;

procedure TForm1.ClearAll1Click(Sender: TObject);
begin
  ClearAll(True);
  if Cleanupfiles then EmptyCurrentDir;
  LoadSequenceList;
end;

procedure TForm1.LoadFilesButtonClick(Sender: TObject);
begin
if (DirEdit.Text <> '') and SysUtils.DirectoryExists(DirEdit.Text) then
  try
    StatusBar1.SimpleText := 'Getting Files and Images. This may take a while...';
    Screen.Cursor := crHourGlass;
    GetDirectoryFiles;
  finally
    Screen.Cursor := crDefault;
    StatusBar1.SimpleText := '';
  end;
end;

procedure TForm1.LoadorReloadFiles1Click(Sender: TObject);
begin
  LoadFiles;
end;

procedure TForm1.LoadSequenceList;
var
  i: Integer;
  PictureImage: TPictureImage;
begin
  SequenceList := TStringList.Create;
  DescriptionList := TStringList.Create;
(**)

  DataModule1.AdsQuery1.Close;
  DataModule1.AdsQuery1.Sql.Text := 'SELECT Description, SequenceNo, RepresentativePicture FROM [' + DataModule1.VehicleSequenceTableName + '] ' +
    'WHERE [SetType] = ''' + LoadType + ''' ORDER BY SequenceNo';
  DataModule1.AdsQuery1.Open;
  if DataModule1.AdsQuery1.IsEmpty  then
  begin
    ShowMessage('Error: ' + DataModule1.VehicleSequenceTableName + ' is empty. Cannot continue without a sequence table');
    DataModule1.AdsQuery1.Close;
    exit;
  end;
  RepresentativePictureNo := 0;
  i := 1;
  while not DataModule1.AdsQuery1.Eof do
  begin
    SequenceList.Add('PicImage' + DataModule1.AdsQuery1.Fields[1].AsString + '=' + DataModule1.AdsQuery1.Fields[1].AsString);
    DescriptionList.Add('PicImage' + DataModule1.AdsQuery1.Fields[1].AsString + '=' + DataModule1.AdsQuery1.Fields[0].AsString);
    if DataModule1.AdsQuery1.Fields[2].AsBoolean then
      RepresentativePictureNo := i;
    inc(i);
    DataModule1.AdsQuery1.Next;
  end;
  if RepresentativePictureNo = 0 then
  begin
    ShowMessage('Error: ' + DataModule1.VehicleSequenceTableName + ' does not have a representative picture. Cannot continue');
    exit;
  end
  else
    DescriptionList.Strings[representativePictureNo - 1] := DescriptionList.Strings[representativePictureNo - 1] + '*';

//  if Loadtype = 'InventoryVehicle' then
//    LoadInventoryVehicle
//  else
//  if LoadType = 'RawMaterials' then
//    LoadRawMaterialsVehicle
//  else
//  if LoadType = 'CartivaTransfer' then
//    LoadCartivaTransferPictures;

  for i := 1 to SequenceList.Count do
  begin
    PictureImage := TPictureImage(Self.FindComponent('PictureImage' + IntToStr(i)));
    if PictureImage <> nil then
    begin
      PictureImage.Image.OnDragOver := Self.PicImage1DragOver;
      PictureImage.Image.OnDragDrop := Self.PicImageDragDrop;
      PictureImage.Image.DragMode := dmAutomatic;
      PictureImage.Image.PopupMenu := PopupMenu1;
      PictureImage.Caption := DescriptionList.Values[DescriptionList.Names[i-1]];
    end
    else
      PictureImage.Caption := '';
  end;
  for i := (SequenceList.Count + 1) to 28 do
  begin
    PictureImage := TPictureImage(Self.FindComponent('PictureImage' + IntToStr(i)));
    if PictureImage <> nil then
    begin
      PictureImage.Image.OnDragOver := nil;
      PictureImage.Image.OnDragDrop := nil;
      PictureImage.Image.DragMode := dmAutomatic;
      PictureImage.Image.PopupMenu := nil;
      PictureImage.Caption := '';
    end

  end;
end;

procedure TForm1.LoadSequenceNames;
var
  i: Integer;
begin
  DataModule1.AdsQuery1.Close;
  DataModule1.AdsQuery1.Sql.Text := 'SELECT SetType, Count(*) FROM VIIPictureSequences GROUP BY SetType ORDER BY 2 DESC;';
  DataModule1.AdsQuery1.Open;
  try
    SetLength(SequenceNames, DataModule1.AdsQuery1.RecordCount);
    i := 0;
    while not DataModule1.AdsQuery1.EOF do
    begin
      if i = 0 then
        DefaultLoadType := DataModule1.AdsQuery1.Fields[0].AsString;
      SetLength(SequenceNames[i], 2);
      SequenceNames[i][0] := DataModule1.AdsQuery1.Fields[0].AsString;
      SequenceNames[i][1] := DataModule1.AdsQuery1.Fields[1].AsString;
      inc(i);
      DataModule1.AdsQuery1.Next;
    end;
  finally
    DataModule1.AdsQuery1.Close;
  end;
end;

procedure TForm1.DeletePicturesfromVehicle1Click(Sender: TObject);
begin
  DoDelete;
end;

procedure TForm1.DisplayImageFullSize1Click(Sender: TObject);
begin
  if not (PopupMenu1.PopupComponent is TImage) then exit;
  TViewer.ShowPicture(TImage(PopupMenu1.PopupComponent));
end;

procedure TForm1.DisplayImageFullSize2Click(Sender: TObject);
begin
 if DiffPictureImage.Visible then
  TViewer.ShowPicture(DiffPictureImage.Image);
end;

procedure TForm1.Exit1Click(Sender: TObject);
begin
  Close;
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
var
  ini: TIniFile;
begin
  ini := TIniFile.Create(Path + 'dirdata.ini');
  try
    ini.WriteString('Directory', 'Pictures', DirEdit.Text);
    ini.WriteString('Advantage', 'LoadType', LoadType);
//    ini.WriteBool('Advantage', 'CleanupAfterUpload', CleanupFiles);
  finally
    ini.Free;
  end;
  SequenceList.Free;
  DescriptionList.Free;
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  ini: TIniFile;
  LoginForm: TLoginForm;
begin
  Path := ExtractFilePath(Application.ExeName);
  ini := TIniFile.Create(Path + 'dirdata.ini');
  try
    DirEdit.Text := ini.ReadString('Directory', 'Pictures', '');
    DirName := DirEdit.Text;
    DataModule1:= TDataModule1.Create(Self);
    DataModule1.VehiclePictureTableName := ini.ReadString('Advantage', 'PictureTable', '');
    DataModule1.VehicleSequenceTableName := ini.ReadString('Advantage', 'SequenceTable', '');
    DataModule1.VehicleUploadsTableName := ini.ReadString('Advantage', 'UploadTable', '');
    DataModule1.Connection := ini.ReadString('Advantage', 'connection', '');
    LoadType := ini.ReadString('Advantage', 'LoadType', 'InventoryVehicle');
    CleanupFiles := ini.ReadBool('Advantage', 'CleanupAfterUpload', False);
    {TODO c: Here we have hard coded the LoadType. If we want to make LoadType data-driven, this logic needs to be updated}
    DataModule1.MakeConnection;

    // Login
    LoginForm := TLoginForm.Create(nil);
    try
      if Loginform.ShowModal = mrOK then
        FUserID := LoginForm.UserID
      else
        Halt;
    finally
      LoginForm.Release;
    end;

    FPictureDirectoryChanged := False;
    if (DirEdit.Text <> '') and SysUtils.DirectoryExists(DirEdit.Text) then
    begin
      DirName := DirEdit.Text;
    end;
  finally
    ini.Free;
  end;
  LoadSequenceNames;
  InitializeSequenceMenuItems;
  ToggleSetTypeMenuItems;
  LoadSequenceList;
  VehicleDescriptionLabel.Caption := NoVehicleSelected;
  DiffPictureImage.Visible := False;
  Self.Show;
end;

procedure TForm1.GetDirectoryFiles;
var
  SearchRec: TSearchRec;
  Filename: String;
  FileCount: Integer;
  CurrentFile: Integer;
  jpg: TJpegImage;
  bmp: TBitmap;
  vscale: Double;
  hscale: Double;
begin
  FileCount := 0;
  CurrentFile := 0;
  LoadedPictureLabel.Caption := '';
  Application.ProcessMessages;
  //Begin by resizing files if necessary
  // cary seems to assume that findfirst might be case sensitive, i'm going to assume not
  // was worried about overwriting the existing file with the resized file, does not seem to be an issue
  // 8-23-12: overwriting is an issue, the pics come from the camera as read only
  // 8/24/12: at detail, something goofy is going on where files in the dir are getting whacked
  //  move the FileSetAttr inside to apply only to the large files
  // though that doesn't feel like a fix at this time
  // need to better understand what is happening
  // seems like it is somehow connected to the goofy camera program
  if FindFirst(DirName + '\*.jp*', faAnyFile, SearchRec) = 0 then
  begin
    repeat
      FileName := DirName + '\' + SearchRec.Name;
//      FileSetAttr(FileName, 0); //remove readonly
      jpg := TJpegImage.Create;
      try
        jpg.LoadFromFile(FileName);
        if jpg.Height > 600 then
        begin
          FileSetAttr(FileName, 0); //remove readonly
          vscale := 600 / jpg.Height;
          if jpg.Width > 800 then
            hscale := 800 / jpg.Width
          else
            hscale := jpg.Width;
          bmp := TBitmap.Create;
          try
            bmp.Width := Round(jpg.Width * hscale);
            bmp.Height:= Round(jpg.Height * vscale);
            bmp.Canvas.StretchDraw(bmp.Canvas.Cliprect, jpg);
            jpg.Assign(bmp);
            jpg.SaveToFile(DirName + '\' + SearchRec.Name);
          finally
            bmp.Free;
          end;
        end;
      finally
        jpg.Free;
      end;
    until FindNext(SearchRec) <> 0;
  end;

  //Begin by counting the total number of files
  if FindFirst(DirName + '\*.*', faAnyFile, SearchRec) = 0 then
  begin
    FileName := SearchRec.Name;
    if (UPPERCASE(ExtractFileExt(Filename)) = '.JPG') or
       (UPPERCASE(ExtractFileExt(FileName)) = '.JPEG') then
      FileCount := FileCount + 1;
  end;
  while FindNext(SearchRec) = 0 do
  begin
    Filename := SearchRec.Name;
    if (UPPERCASE(ExtractFileExt(Filename)) = '.JPG') or
       (UPPERCASE(ExtractFileExt(FileName)) = '.JPEG') then
      FileCount := FileCount + 1;
  end;

  if FileCount > 0 then
  begin
    ListBox1.Clear;
    PictureImages.Clear;
    if FindFirst(DirName + '\*.*', faAnyFile, SearchRec) = 0 then
    begin
      Filename := SearchRec.Name;
      if (UPPERCASE(ExtractFileExt(Filename)) = '.JPG') or
         (UPPERCASE(ExtractFileExt(FileName)) = '.JPEG') then
      begin
        Application.ProcessMessages;
        PictureImages.Add(ResizeJPG2BMP(DirName + '\' + SearchRec.Name, wConst, hConst),nil);
        ListBox1.Items.Add(SearchRec.Name);
        CurrentFile := CurrentFile + 1;
      end;
      while FindNext(SearchRec) = 0 do
      begin
        Application.ProcessMessages;
        Filename := SearchRec.Name;
        if (UPPERCASE(ExtractFileExt(Filename)) = '.JPG') or
           (UPPERCASE(ExtractFileExt(FileName)) = '.JPEG') then
        begin
          PictureImages.Add(ResizeJPG2BMP(DirName + '\' + SearchRec.Name, wConst, hConst),nil);
          ListBox1.Items.Add(SearchRec.Name);
          CurrentFile := CurrentFile + 1;
        end;
      end;
    end;
    LoadedPictureLabel.Caption := IntToStr(FileCount) + ' files loaded';
  end;
end;

function TForm1.GetIntFromPicImage(Name: String): Integer;
begin
  Result := StrToInt(Copy(Name, 9, 3));
end;

function TForm1.get_PictureDirectory: String;
begin
  Result := DirEdit.Text;
end;

procedure TForm1.InitializeSequenceMenuItems;
var
 i: Integer;
 MenuItem: TMenuItem;
begin
  for i := low(SequenceNames) to high(SequenceNames) do
  begin
    MenuItem := TMenuItem.Create(Self);
    MenuItem.Caption := MakeReadableMenuName(SequenceNames[i][0]) + ' ('+ SequenceNames[i][1] + ' Pictures)';;
    MenuItem.Name := 'Menu' + SequenceNames[i][0];
    MenuItem.OnClick := SequenceMenuItemClick;
    MainMenu1.Items[1].Add(MenuItem);
  end;
end;

function TForm1.JPegToTPicture(JPegName: String;
  out Picture: TPicture): Boolean;
  var
    Stream: TMemoryStream;
    Jpg: TJpegImage;
    Bitmap: TBitmap;
    p : TPicture;
const
  h = 24;
  w = 24;
begin
  Result := False;
   Jpg := nil;
    Stream := nil;
    p := TPicture.Create;
   try
      Stream := TMemoryStream.Create;
      Stream.LoadFromFile(JPegName);
      if Stream.Size > 0
       then begin
              Jpg := TJpegImage.Create;
              Stream.Position := 0;
              Jpg.LoadFromStream(Stream);
              p.Assign(Jpg);
              Bitmap := TBitmap.Create;
              Bitmap.Height := h;
              Bitmap.Width := w;
              p.Bitmap.Canvas.StretchDraw(Rect(0,0,w,h), Bitmap);
//            p.Bitmap.Canvas.StretchDraw(Rect(0,0,100,80), Picture.Graphic);
              Picture.Bitmap := Bitmap;
              Result := True;
           end
       else Picture.Assign(nil);
    except
//      Picture.Assign(nil);
    end;
    jpg.Free;
    Stream.Free;
end;

procedure TForm1.ListBox1DrawItem(Control: TWinControl; Index: Integer;
  Rect: TRect; State: TOwnerDrawState);
var
  CenterText : integer;
begin
  ListBox1.Canvas.FillRect (rect);
  // now draw
  PictureImages.Draw(ListBox1.Canvas,rect.Left + 4, rect.Top + 4, index );
  // you have to center the text vertically besidethe bitmap, or it will appear a little heigher
  CenterText := ( rect.Bottom - rect.Top - ListBox1.Canvas.TextHeight(text)) div 2 ;
  ListBox1.Canvas.TextOut (rect.left + PictureImages.Width + 8 , rect.Top + CenterText,
  ListBox1.Items.Strings[index]);
end;

procedure TForm1.ListBox1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  idx: Integer;
begin
  if Key = vk_Delete then
  begin
    idx := ListBox1.ItemIndex;
    if idx = -1 then
      exit
    else
    begin
      ListBox1.Items.Delete(idx);
      PictureImages.Delete(idx);
      if ListBox1.Count > 0 then
        if idx >= ListBox1.Count then
          ListBox1.ItemIndex := ListBox1.Count -1
        else
          ListBox1.ItemIndex := idx;
    end;
  end;
end;

procedure TForm1.ListBox1MeasureItem(Control: TWinControl; Index: Integer;
  var Height: Integer);
begin
  height := PictureImages.Height + 4;
end;

procedure TForm1.PicImage1DragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
if (Sender is TListBox) or (Sender is TImage) then
  Accept := True;
end;

procedure TForm1.PicImageDragDrop(Sender, Target: TObject; X, Y: Integer);
var
  HoldImageName: String;
begin
  if Target = Sender then exit;
  if Target is TImage then
  begin
    if PictureList[GetIntFromPicImage(TComponent(Target).Name)] = '' then exit;
    HoldImageName := PictureList[GetIntFromPicImage(TComponent(Sender).Name)];
    PictureList[GetIntFromPicImage(TComponent(Sender).Name)] := PictureList[GetIntFromPicImage(TComponent(Target).Name)];
    PictureList[GetIntFromPicImage(TComponent(Target).Name)] := HoldImageName;
    TImage(Sender).Picture.LoadFromFile(PictureList[GetIntFromPicImage(TComponent(Sender).Name)]);
    if HoldImageName <> '' then
      Timage(Target).Picture.LoadFromFile(PictureList[GetIntFromPicImage(TComponent(Target).Name)])
    else
      Timage(Target).Picture := nil;
  end
  else
  if Target is TListBox then
  begin
    if TListBox(Target).ItemIndex = -1 then exit;

    PictureList[GetIntFromPicImage(TComponent(Sender).Name)] := DirEdit.Text + '\' + TListBox(Target).Items[TListBox(Target).ItemIndex];
    TImage(Sender).Picture.LoadFromFile(PictureList[GetIntFromPicImage(TComponent(Sender).Name)]);
    PictureImages.Delete(TListBox(Target).ItemIndex);
    TListBox(Target).Items.Delete(TListBox(Target).ItemIndex);
  end;
end;


procedure TForm1.PopupMenu1Popup(Sender: TObject);
var
  Image: TImage;
begin
  if TPopupMenu(Sender).PopupComponent is TImage then
  begin
    Image := TImage(TPopupMenu(Sender).PopupComponent);
    DisplayImageFullSize1.Visible := (PictureList[GetIntFromPicImage(Image.Name)] <> '');
    RemoveImage1.Visible := DisplayImageFullSize1.Visible;
    RemoveAllImages1.Visible := DisplayImageFullSize1.Visible;
  end;
end;


procedure TForm1.RecycleBinDragDrop(Sender, Source: TObject; X, Y: Integer);
var
  idx: Integer;
  name: String;
begin
  if Source = Sender then exit;
  if Source is TImage then
  begin
    Name := PictureList[GetIntFromPicImage(TComponent(Source).Name)];
    ListBox1.Items.Add(ExtractFileName(PictureList[GetIntFromPicImage(TComponent(Source).Name)]));
    PictureImages.Add(ResizeJPG2BMP(PictureList[GetIntFromPicImage(TComponent(Source).Name)], wConst, hConst),nil);
    //TODO: Add the image back to the PictureImages ImageList
    PictureList[GetIntFromPicImage(TComponent(Source).Name)] := '';
    TImage(Source).Picture := nil;
  end
  else
  if Source is TListBox then
  begin
    idx := TListBox(Source).ItemIndex;
    TListBox(Source).Items.Delete(idx);
    PictureImages.Delete(idx);
  end;

end;

procedure TForm1.RecycleBinDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  if (Source is TImage) or (Source is TListBox) then
    Accept := True;
end;

procedure TForm1.RemoveAllImages1Click(Sender: TObject);
var
  picnumber: Integer;
  ImageRef: TImage;
begin
  for picnumber := low(PictureList) to high(PictureList) do
  begin
    if PictureList[picnumber] <> '' then
    begin
      ImageRef := TPictureImage(Self.FindComponent('PictureImage' + IntToStr(picnumber))).Image;
      PictureImages.Add(ResizeJPG2BMP(PictureList[picnumber], wConst, hConst),nil);
      ListBox1.Items.Add(ExtractFileName(PictureList[picnumber]));
      ImageRef.Picture := nil;
      PictureList[picnumber] := '';
    end;
  end;
end;

procedure TForm1.RemoveImage1Click(Sender: TObject);
var
  picnumber: Integer;
  ImageRef: TImage;
begin
  ImageRef := TImage(TPopupMenu(TMenuItem(Sender).GetParentMenu).PopupComponent);
  picnumber := GetIntFromPicImage(ImageRef.Name);
  PictureImages.Add(ResizeJPG2BMP(PictureList[picnumber], wConst, hConst),nil);
  ListBox1.Items.Add(ExtractFileName(PictureList[picnumber]));
  ImageRef.Picture := nil;
  PictureList[picnumber] := '';
end;

procedure TForm1.SelectVehicleButtonClick(Sender: TObject);
begin
  if not SysUtils.DirectoryExists(DirName) then
  begin
    ShowMessage('Picture directory ' + DirName + ' does not exist. Select a valid directory and try again');
    exit;
  end;
  if LookupVehicle then
    ProcessVehicle
end;

procedure TForm1.SequenceMenuItemClick(Sender: TObject);
begin
  Loadtype := Copy(TMenuItem(Sender).Name, 5, 255);
  ToggleSetTypeMenuItems;
  ClearAll;
  LoadSequenceList;
end;

procedure TForm1.ProcessVehicle;
var
  PicField: TBlobField;
  OrderField: TIntegerField;
  DescField: TStringField;
  PictureImage: TPictureImage;
  stmt: String;
begin
  ClearAll;
  VehicleDescriptionLabel.Caption := FCurrentDescription;

  stmt := 'SELECT SetType FROM [' + DataModule1.VehicleUploadsTableName + ']' +
          '   vutn WHERE vutn.VehicleInventoryItemID = ''' + FCurrentVehicleInventoryItemID + '''';
  DataModule1.AdsQuery1.SQL.Text := stmt;
  DataModule1.AdsQuery1.Open;
  try
    VehicleWithExistingPictures := DataModule1.AdsQuery1.RecordCount > 0;
    DeletePicturesfromVehicle1.Enabled := VehicleWithExistingPictures;
    if VehicleWithExistingPictures then
    begin
      //There are existing records. get their SetType, toggle the SetType menu items, and then load the set
      //Load the SetType. If SetType is blank (rare), choose the LoadType that has the most pictures
      if (DataModule1.AdsQuery1.FieldByName('SetType').AsString = '') then
        LoadType := DefaultLoadType
      else
        LoadType := DataModule1.AdsQuery1.FieldByName('SetType').AsString;
    end;
  finally
    DataModule1.AdsQuery1.Close;
  end;
  ToggleSetTypeMenuItems;
  LoadSequenceList;
  //Now, get any existing pictures that need to be loaded into the image panel
  stmt := 'SELECT vip.* ' +
         ' FROM ' + DataModule1.VehiclePictureTableName + ' vip ' +
         ' WHERE vip.VehicleInventoryItemID = ''' + FCurrentVehicleInventoryItemID + '''';
  DataModule1.AdsQuery1.SQL.Text := stmt;
  DataModule1.AdsQuery1.Open;
  try
    StatusBar1.SimpleText := 'Loading pictures';
    Screen.Cursor := crHourGlass;
    Application.ProcessMessages;
    PicField := TBlobField(DataModule1.AdsQuery1.FieldByName('Picture'));
    DescField := TStringField(DataModule1.AdsQuery1.FieldByName('Description'));
    OrderField := TIntegerField(DataModule1.AdsQuery1.FieldByName('SequenceNo'));
    while not DataModule1.AdsQuery1.EOF do
    begin
      PicField.SaveToFile(DirName + '\' + FCurrentVin + '_' + SysUtils.Format('%2.2d', [OrderField.AsInteger]) + '.jpg');
      PictureList[OrderField.AsInteger] := DirName + '\' + FCurrentVin + '_' + SysUtils.Format('%2.2d', [OrderField.AsInteger]) + '.jpg';
//TODO: The following is a call. Used to be that we loaded the descriptions associated with the picture. Now we load the current sequence description
      StatusBar1.SimpleText := 'Loading ' + DescField.AsString + '...';
      Application.ProcessMessages;
      PictureImage := TPictureImage(Self.FindComponent('PictureImage' + IntToStr(OrderField.AsInteger)));
      if PictureImage <> nil then
      begin
        PictureImage.Image.Picture.LoadFromFile(PictureList[OrderField.AsInteger]);
        PictureImage.Caption := DescriptionList.values[DescriptionList.Names[OrderField.AsInteger - 1]];
      end;
      DataModule1.AdsQuery1.Next;
    end;
  finally
    Screen.Cursor := crDefault;
    StatusBar1.SimpleText := '';
  end;
  //GetDirectoryFiles;
  DataModule1.AdsQuery1.Close;
  Upload1.Enabled := True;
  LoadDifferentiator;
  LoadFiles;
end;

function TForm1.LookupVehicle: Boolean;
begin
  Result := False;
  with TSelectVehicleForm.Create(nil) do
  begin
    try
      if ShowModal = mrOK then
      begin
        //These variables are being loaded, but the user may subsequently select cancel to the load pictures
        FCurrentVIN := DataModule1.AdsQuery1.FieldByName('VIN').AsString;
        FCurrentVehicleInventoryItemID := DataModule1.AdsQuery1.FieldByName('VehicleInventoryItemID').AsString;
        FCurrentVehicleItemID := DataModule1.AdsQuery1.FieldByName('VehicleItemID').AsString;
        FCurrentDescription := DataModule1.AdsQuery1.FieldByName('Description').AsString;
        //ShowMessage(FCurrentVIN);
        Result := True;
      end;
    finally
      DataModule1.AdsQuery1.Close;
      Release;
    end;
  end;
end;

function TForm1.MakeReadableMenuName(const Name: String): String;
var
  i: Integer;
begin
  Result := Name[1];
  for i := 2 to Length(Name) do
    if Character.IsUpper(Name[i]) then
      Result := Result + ' ' + Name[i]
    else
      Result := Result + Name[i];
end;

procedure TForm1.DoDelete;
var
  DeletePictures: TDeletePictures;
begin
  if FCurrentVehicleInventoryItemID = '' then
  begin
    //This should never, ever happen
    ClearAll(True);
    raise Exception.Create('Error: No vehicle selected. Cannot continue');
  end;
  DeletePictures := TDeletePictures.Create(nil);
  try
    if DeletePictures.ShowModal('Pictures for ' + VehicleDescriptionLabel.Caption +
      ' are about to be permanentaly deleted. Select OK to delete or Cancel to cancel') = mrOK then
    begin
      DataModule1.AdsQuery1.Close;
      DataModule1.AdsQuery1.SQL.Text := 'DELETE FROM ' + DataModule1.VehiclePictureTableName + ' WHERE VehicleInventoryItemID = ''' + FCurrentVehicleInventoryItemID + '''';
      DataModule1.AdsQuery1.ExecSQL;
      ClearAll(True);
      LoadSequenceList;
      if Self.CleanupFiles then
        EmptyCurrentDir;
    end;
  finally
    DeletePictures.Release;
  end;

end;

procedure TForm1.ManageRydellDifferentiatorPictures1Click(Sender: TObject);
begin
  with TDifferentiator.Create(nil) do
  try
    ShowModal;
    LoadDifferentiator;
  finally
    Release;
  end;
end;

procedure TForm1.set_PictureDirectory(Value: String);
begin
  if Value <> DirEdit.Text then
  begin
    DirEdit.Text := Value;
    FPictureDirectoryChanged := True;
  end;
end;

procedure TForm1.ToggleSetTypeMenuItems;
var
  i: Integer;
  MenuItem: TMenuItem;
begin
  for i := low(SequenceNames) to high(SequenceNames) do
  begin
    MenuItem := TMenuItem(Self.FindComponent('Menu' + SequenceNames[i][0]));
    if MenuItem <> nil then
      MenuItem.Checked := False;
  end;
  MenuItem := TMenuItem(Self.FindComponent('Menu' + LoadType));
  if MenuItem <> nil then
    MenuItem.Checked := True;
//  TMenuItem(Sender).Checked := True;


//  if LoadType = 'InventoryVehicle' then
//  begin
//    InventoryVehicle281.Checked  := True;
//    RawMaterialsVehicle8pictures1.Checked := False;
//    CartivaTransfer14pictures1.Checked := False;
//  end
//  else if LoadType = 'CartivaTransfer' then
//  begin
//    CartivaTransfer14pictures1.Checked := True;
//    InventoryVehicle281.Checked  := False;
//    RawMaterialsVehicle8pictures1.Checked := False;
//  end
//  else if LoadType = 'RawMaterials' then
//  begin
//    InventoryVehicle281.Checked  := False;
//    RawMaterialsVehicle8pictures1.Checked := True;
//    CartivaTransfer14pictures1.Checked := False;
//  end
end;

procedure TForm1.LoadDifferentiator;
var
  PicFld: TBlobField;
  ms: TMemoryStream;
  Jpg: TJpegImage;
begin
  DataModule1.AdsQuery1.Close;
  DataModule1.AdsQuery1.SQL.Text := 'EXECUTE PROCEDURE GetDifferentiatorForVehicleInventoryItemID('''+FCurrentVehicleInventoryItemID + ''')';
  DataModule1.AdsQuery1.Open;
  try
    PicFld := TBlobField(DataModule1.AdsQuery1.Fields[0]);
    if not PicFld.IsNull then
    begin
      ms := TMemoryStream.Create;
      try
        Jpg := TJpegImage.Create;
        try
          PicFld.SaveToStream(ms);
          ms.Position := 0;
          Jpg.LoadFromStream(ms);
          DiffPictureImage.Image.Picture.Assign(jpg);
          DiffPictureImage.Visible := True;
        finally
          Jpg.Free;
        end;
      finally
        ms.Free;
      end;
    end;
  finally
     DataModule1.AdsQuery1.Close;
  end;
end;

procedure TForm1.DoUpload;
var
  i: Integer;
  NoPictures: Boolean;
  Val: Integer;
begin
  if FCurrentVIN = '' then
  begin
    ShowMessage('Select a vehicle before attempting to upload pictures');
    exit;
  end;

  //Determine if no pictures are in the upload set
  NoPictures := True;
  for i := Low(PictureList) to High(PictureList) do
  begin
    if PictureList[i] <> '' then
    begin
      NoPictures := False;
      break;
    end;
  end;

  if NoPictures then
  begin
    ShowMessage('No pictures to upload. Cannot continue');
    exit;
  end;

  if (PictureList[RepresentativePictureNo] = '') then
  begin
    ShowMessage('The representative picture is missing. You cannot upload without the picture ' +
      DescriptionList.Values[IntToStr(RepresentativePictureNo)]);
    exit;
  end;


  ConfirmUpload := TConfirmUpload.Create(nil);
  try
    ConfirmUpload.Image1.Picture.LoadFromFile(PictureList[RepresentativePictureNo]);
    ConfirmUpload.VehicleDescription.Caption := VehicleDescriptionLabel.Caption;
    if VehicleWithExistingPictures then
      val := ConfirmUpload.ShowModal('Replace existing pictures with the current pictures? Select OK to replace or Cancel to cancel')
    else
      val :=ConfirmUpload.ShowModal('Confirm that you want to upload these pictures. Select OK to upload or Cancel to cancel');
  finally
    ConfirmUpload.Release;
  end;

  if val <> mrOK then exit;

  //val = mrOK
  if VehicleWithExistingPictures then
  begin
    DataModule1.AdsQuery1.Close;
    DataModule1.AdsQuery1.SQL.Text := 'DELETE FROM ' + DataModule1.VehiclePictureTableName +
                                      ' WHERE VehicleInventoryItemID = ''' + FCurrentVehicleInventoryItemID + '''';
    DataModule1.AdsQuery1.ExecSQL;
  end;

  //This is the beginning of the upload
  DataModule1.AdsQuery1.Close;
  DataModule1.AdsTable1.Close;
  Screen.Cursor :=  crHourGlass;
  try
  DataModule1.AdsConnection1.BeginTransaction;
    try
      DataModule1.AdsTable1.TableName := DataModule1.VehiclePictureTableName;
      DataModule1.AdsTable1.Open;
      for i := Low(PictureList) to High(PictureList) do
        if PictureList[i] <> '' then
        begin
          StatusBar1.SimpleText := 'Uploading ' + DescriptionList.Values[SequenceList.Names[i-1]];
          Application.ProcessMessages;
          DataModule1.AdsTable1.Append;
          DataModule1.AdsTable1.FieldByName('VehicleInventoryItemID').AsString := FCurrentVehicleInventoryItemID;
          TBlobField(DataModule1.AdsTable1.FieldByName('Picture')).LoadFromFile(PictureList[i]);
          DataModule1.AdsTable1.FieldByName('Description').AsString := DescriptionList.Values[SequenceList.Names[i-1]];
          DataModule1.AdsTable1.FieldByName('SequenceNo').AsString := SequenceList.Values[SequenceList.Names[i-1]];
          DataModule1.AdsTable1.Post;
        end;
      DataModule1.AdsTable1.Close;
      DataModule1.AdsQuery1.Close;
      DataModule1.AdsQuery1.SQL.Text :=
        'MERGE '+ DataModule1.VehicleUploadsTableName + ' viiu ' +
        '  ON ( viiu.VehicleInventoryItemID = :vii ) ' +
        'WHEN MATCHED THEN ' +
        'UPDATE ' +
        '  SET viiu.SetType = :LoadType, ' +
        '  viiu.UploadTS = Current_Timestamp(), ' +
        '  viiu.UploadedBy = :user ' +
        'WHEN NOT MATCHED THEN ' +
        'INSERT (VehicleInventoryItemID, UploadTS, SetType, UploadedBy ) ' +
        '  VALUES ( :vii, Current_Timestamp(), :LoadType, :user )';
      DataModule1.AdsQuery1.Params[0].AsString := FCurrentVehicleInventoryItemID;
      DataMOdule1.AdsQuery1.Params[1].AsString := LoadType;
      DataModule1.AdsQuery1.Params[2].AsString := FUserID;
      DataModule1.AdsQuery1.ExecSQL;
      //upload complete

      if CleanupFiles then
        EmptyCurrentDir;

      ClearAll(True);
      LoadSequenceList;
      DataModule1.AdsConnection1.Commit;
    except
      DataModule1.AdsConnection1.Rollback;
    end;
  finally
    StatusBar1.SimpleText := '';
    Screen.Cursor := crDefault;
  end;

end;

procedure TForm1.Upload1Click(Sender: TObject);
begin
  DoUpload;
end;

end.
