unit DeletePicturesu;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TDeletePictures = class(TForm)
    Label1: TLabel;
    CheckBox1: TCheckBox;
    Button1: TButton;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function ShowModal(Msg: String): Integer; overload;
  end;

var
  DeletePictures: TDeletePictures;

implementation

uses mainformu;

{$R *.dfm}

{ TUploadComplete }

procedure TDeletePictures.Button1Click(Sender: TObject);
begin
  Form1.CleanupFiles := CheckBox1.Checked;
  Self.ModalResult := mrOK;
end;

function TDeletePictures.ShowModal(Msg: String): Integer;
begin
  Label1.Caption := Msg;
  CheckBox1.Checked := Form1.CleanupFiles;
  CheckBox1.Caption := 'Delete files from ' + mainformu.DirName;
  Result := Self.ShowModal;
end;

end.
