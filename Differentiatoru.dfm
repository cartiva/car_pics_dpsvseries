object Differentiator: TDifferentiator
  Left = 0
  Top = 0
  Caption = 'Rydell Differentiator Pictures'
  ClientHeight = 570
  ClientWidth = 844
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  DesignSize = (
    844
    570)
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 349
    Top = 101
    Width = 475
    Height = 422
    Anchors = [akLeft, akTop, akRight, akBottom]
    ExplicitWidth = 316
    ExplicitHeight = 217
  end
  object Label1: TLabel
    Left = 349
    Top = 40
    Width = 53
    Height = 13
    Caption = 'Description'
  end
  object DBGrid1: TDBGrid
    Left = 8
    Top = 32
    Width = 320
    Height = 530
    Anchors = [akLeft, akTop, akBottom]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object Edit1: TEdit
    Left = 349
    Top = 63
    Width = 254
    Height = 21
    TabOrder = 1
  end
  object Button1: TButton
    Left = 459
    Top = 32
    Width = 75
    Height = 25
    Caption = 'Select Picture'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 688
    Top = 537
    Width = 130
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Add To Database'
    TabOrder = 3
    OnClick = Button2Click
    ExplicitLeft = 535
    ExplicitTop = 333
  end
  object DataSource1: TDataSource
    Left = 220
    Top = 90
  end
  object OpenPictureDialog1: TOpenPictureDialog
    Filter = 'JPEG Files (*.jpg, *.jpeg)|*.jpg;*.jpeg'
    Left = 219
    Top = 166
  end
end
